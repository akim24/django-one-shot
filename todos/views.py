from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list": lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": details,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list_list = form.save()
            return redirect("todo_list_detail", id=(todo_list_list.id))
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todo_list_list = form.save()
            return redirect("todo_list_detail", id=(todo_list_list.id))
    else:
        form = TodoListForm(instance=todolist)
        context = {
            "todo_list_object": todolist,
            "todo_list_form": form,
        }
        return render(request, "todos/update.html", context)

def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_list_list = form.save()
            return redirect("todo_list_detail", id=(todo_list_list.list.id))
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/item_create.html", context)

def todo_item_update(request, id):
    todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=(todo_item.list.id))
    else:
        form = TodoItemForm(instance=todoitem)
        context = {
            "todo_item_object": todoitem,
            "todo_item_form": form,
        }
        return render(request, "todos/item_update.html", context)
